# VPC
# variable "main_vpc_id" {
#   description = "Main VPC ID"
#   type        = string
# }



variable "region" {
  description = "Default region for our VPC"
  type        = string
}





# SUBNET
# PUBLIC
# variable "public_subnet_id" {
#   description = "CIDR of the public Subnet in AZ A"
#   type        = string
# }

# variable "private_subnet_id" {
#   description = "CIDR of the public Subnet in AZ A"
#   type        = string
# }



# INSTANCE
variable "instance_type" {
  description = "The type of my first server"
  type        = string
  default     = "t2.medium"
}

# variable "keypair_name" {
#   description = "Name of my key pair on the console"
#   type        = string
# }