terraform {
  backend "s3" {
    bucket = "elk-stack-team2"
    key    = "challange/bastioninstance/terraform.tfstates"
  }
}