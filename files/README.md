# ELK STACK 

*Thanks for visiting!!*. [TEAM 2 | Talent Academy](https://cloudreach.jira.com/wiki/spaces/CE/pages/3955621898/Team+2+-+ELK+Stack)

This Markdown cheat sheet provides a quick overview of Team 2 challenge of ELK stack for log analyser project elements.
 
### What is the ELK Stack?

The ELK Stack was a collection of three open-source products — Elasticsearch, Logstash, and Kibana — all developed, managed and maintained by Elastic.
Elasticsearch is an open source, full-text search and analysis engine, based on the Apache Lucene search engine. Logstash is a log aggregator that collects data from various input sources, executes different transformations and enhancements and then ships the data to various supported output destinations. Kibana is a visualization layer that works on top of Elasticsearch, providing users with the ability to analyze and visualize the data.
More information is available [here](https://logz.io/learn/complete-guide-elk-stack/#intro).

### Use Case : 

>Design and develop the log analysis stack named ELK. It is comprised of the Elastricsearch app for indexing, the Logstash app for log gathering server and Kibana for the dashboard and search filter.
Also need to deploy a couple of test servers to collect/forward their system logs to and demonstrate the ability to filter on the Kibana board.

### Solution : 
>The main focus is to deploy 3 AWS EC2 servers which will each host one of the application stack. Each application needs to be deployed through configuration management, using Ansible. Consider each part of the stack to be individual application, which in this case would be hosted in invididual git repository.

<br>

# Architecture Design Document

V 1.0


<img src="files/ELK_Challange.png" width=75% height=75%>

<br>

### VPC Elements

| Element | Description |
| ----------- | ----------- |
| CIDR | 192.168.0.0/16 |
| Subnets | /24 |
| Kibana Subnet | 192.168.1.0/24 |
| Monitering Subnet | 192.168.2.0/24 |
| Demo Subnet | 192.168.3.0/24 |

<br>

### Security groups
*Logstash-security-group*

| Description | IP Version | Type | Protocol | Port range | Source |
| ----------- | ---------- | ---- | -------- | ---------- | ------ |
| Communication | IPv4 | Custom TCP | TCP | 9300 | 0.0.0.0/0 |
| SSH | IPv4 | SSH | TCP | 22 | 0.0.0.0/0 |
| Logstash | IPv4 | Custom TCP | TCP | 9600 | 0.0.0.0/0 |
| Requests | IPv4 | Custom TCP | TCP | 9200 | 0.0.0.0/0 |

<br>


*Elasticsearch-security-group*

| Description | IP Version | Type | Protocol | Port range | Source |
| ----------- | ---------- | ---- | -------- | ---------- | ------ |
| Kibana | IPv4 | Custom TCP | TCP | 5601 | 0.0.0.0/0 |
| Logstash | IPv4 | Custom TCP | TCP | 5044 | 0.0.0.0/0 |
| SSH | IPv4 | SSH | TCP | 22 | 0.0.0.0/0 |
| Communication | IPv4 | Custom TCP | TCP | 9300 | 0.0.0.0/0 | 
| Requests | IPv4 | Custom TCP | TCP | 9200 | 0.0.0.0/0 |

<br>

*Kibana-security-group*

| Description | IP Version | Type | Protocol | Port range | Source |
| ----------- | ---------- | ---- | -------- | ---------- | ------ |
| Kibana | IPv4 | TCP | TCP | 5601 | 0.0.0.0/0 |
| Requests | IPv4 | TCP | TCP | 9200 | 0.0.0.0/0 |
| Communication | IPv4 | Custom TCP | TCP | 9300 | 0.0.0.0/0 | 
| Communication | IPv4 | SSH | TCP | 22 | 192.168.1.224/24 | 
| Web | IPv4 | HHTP | TCP | 80 | work ip addresses |  
| Web | IPv4 | HTTPS | TCP | 443 | work ip addresses |

<br>

*Bastian-security-group*

| Description | IP Version | Type | Protocol | Port range | Source |
| ----------- | ---------- | ---- | -------- | ---------- | ------ |
| Communication | IPv4 | SSH | TCP | 22 | work ip addresses | 
| Web | IPv4 | HHTP | TCP | 80 | work ip addresses |  
| Web | IPv4 | HTTPS | TCP | 443 | work ip addresses |

<br>
<br>




---

### Terraform Code - VPC

```py
module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = "192.168.0.0/16"

  azs             = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  private_subnets = ["192.168.2.0/24", "192.168.3.0/24"]
  public_subnets  = ["192.168.1.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}

sample code goes here ------!
```


<br>

---

### Cloudreach Talent Academy 2021

[cloudreach.com](https://www.cloudreach.com/en/aws-talent-academy/)

<img src="files/TA_Logo.png" width=10% height=10%>

