#------Bastion sec group----

resource "aws_security_group" "bastion-security-group" {
  name        = "bastion-security-group"
  description = "Allow access to my Server"
  vpc_id      = data.aws_vpc.elk_vpc_id.id

  # INBOUND RULES
  ingress {
    description = "SSH from work"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "RDP from work "
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "SSH from bastion"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "SSH from bastion"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "PING to bastion"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["192.168.0.0/24"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "bastion-security-group"
  }
}

data "aws_subnet" "bastion_id" {
  filter {
    name   = "tag:Name"
    values = ["Kibana subnet"]
  }
}

# ---------------------------------EC2--------------------------------------

# EC2 - bastion
resource "aws_instance" "bastion-server" {
  ami                         = data.aws_ami.my_aws_ami.id
  instance_type               = var.instance_type
  key_name                    = data.aws_key_pair.keypair_name.key_name
  subnet_id                   = data.aws_subnet.bastion_id.id
  vpc_security_group_ids      = [aws_security_group.bastion-security-group.id]
  associate_public_ip_address = true

  user_data = <<-EOF
                 #!/bin/bash
                  ########################################
                  ##### USE THIS WITH AMAZON LINUX 2 #####
                  ########################################

                  sudo apt-get update -y
                  sudo apt-get upgrade -y
                  
                
                 EOF

  tags = {
    Name = "bastion_server"
  }
}