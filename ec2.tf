#Data Source for VPC -----------------------
data "aws_vpc" "elk_vpc_id" {
  filter {
    name   = "tag:Name"
    values = ["challenge week VPC"]
  }
}
# --------------------------------------------
resource "aws_security_group" "demo-security-group" {
  name        = "demo-security-group"
  description = "Allow access to my Server"
  vpc_id      = data.aws_vpc.elk_vpc_id.id

  # INBOUND RULES
  ingress {
    description = "SSH from Bastian"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["192.168.1.0/24"]
  }

  ingress {
    description = "Logstash comm "
    from_port   = 9300
    to_port     = 9300
    protocol    = "tcp"
    cidr_blocks = ["192.168.2.0/24"]
  }

  ingress {
      description = "Logstash comm "
      from_port   = 5044
      to_port     = 5044
      protocol    = "tcp"
      cidr_blocks = ["192.168.2.0/24"]
    }

    ingress {
      description = "Logstash comm "
      from_port   = 9600
      to_port     = 9600
      protocol    = "tcp"
      cidr_blocks = ["192.168.2.0/24"]
    }

    ingress {
      description = "Logstash comm "
      from_port   = 9200
      to_port     = 9200
      protocol    = "tcp"
      cidr_blocks = ["192.168.2.0/24"]
    }

    ingress {
      description = "RDP from Bastian "
      from_port   = 3389
      to_port     = 3389
      protocol    = "tcp"
      cidr_blocks = ["192.168.1.0/24"]
    }

    ingress {
      description = "tcp"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["192.168.2.0/24"]
    }

    ingress {
      description = "PING from web"
      from_port   = 0
      to_port     = 0
      protocol    = "icmp"
      cidr_blocks = ["192.168.0.0/16"]
    }

    egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "demo-security-group"
  }
}

#-------- AMI --------

data "aws_ami" "my_aws_ami" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# --------- Data source ------------------------------------------------
#Data Source for Subnet
data "aws_subnet" "demo" {
  filter {
    name   = "tag:Name"
    values = ["Demo subnet"]
  }
}

output "name" {
  value = data.aws_key_pair.keypair_name.key_name
}

# ---------------------------------EC2--------------------------------------
# EC2 - Demo servers
resource "aws_instance" "demo_servers" {
  ami = data.aws_ami.my_aws_ami.id
  instance_type               = var.instance_type
  key_name                    = var.keypair_name
  subnet_id                   = data.aws_subnet.demo.id
  vpc_security_group_ids      = [aws_security_group.demo-security-group.id]
  associate_public_ip_address = false

  user_data = <<-EOF
                 #!/bin/bash
                  ########################################
                  ##### USE THIS WITH AMAZON LINUX 2 #####
                  ########################################

                  sudo apt-get update -y
                  sudo apt-get upgrade -y
                  sudo apt install logstash
                  sudo systemctl start logstash
                  sudo systemctl enable logstash
                  sudo apt install filebeat
                  sudo filebeat modules enable system
                  sudo filebeat setup --pipelines --modules system
                  sudo filebeat setup --index-management -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]'
                  sudo filebeat setup -E output.logstash.enabled=false -E output.elasticsearch.hosts=['localhost:9200'] -E setup.kibana.host=localhost:5601
                  sudo systemctl start filebeat
                  sudo systemctl enable filebeat
                
                 EOF

  tags = {
    Name = "Demo1_server"
  }
}