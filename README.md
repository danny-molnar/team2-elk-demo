# Use a SSH Jump Host With Ansible

*Bastion Host*. [TEAM 2 | Talent Academy](https://cloudreach.jira.com/wiki/spaces/CE/pages/3955621898/Team+2+-+ELK+Stack)

In this post will demonstrate how to use a SSH Bastion or Jump Host with Ansible to reach the target server.
In some scenarios, the target server might be in a private range which is only accessible via a bastion host, and that counts the same for ansible as ansible is using SSH to reach to the target servers.
 

<br>

### SSH Config , Using a Bastion with Ansible

Our bastion host is configured as bastion and all other private servers config under ~/.ssh/config looks like this:

<br>

![Getting Started](files/ssh_config.png)


<br>
<br>

To verify that our config is working, you should be able to use:

<br>

![Getting Started](files/ssh_bastion.png)

<br>

To ssh private instances : 

<br>

![Getting Started](files/ssh_demo.png)

